# Standarisasi Web Can Creative
Standarisasi ini akan digunakan mulai tahun 2021 yaitu setelah server
diupdate menggunakan versi PHP8.x atau lebih tinggi dan akan diupdate seperlunya,
yang kemudian akan digunakan sebagaimana semestinya.
<br><br>
## Tools
* Laravel v8.x
* PHP v8.x
* Vue v2.x

## Convention
1. Laravel Controller
    * Gunakan Model Eloquent
    * Selalu validasi tiap `request`!
    * Selalu gunakan `migrations`!
    * Manfaatkan [event](https://laravel.com/docs/8.x/eloquent#events-using-closures) 
    eloquent atau [observer](https://laravel.com/docs/8.x/eloquent#observers)
    * Manfaatkan [scope](https://laravel.com/docs/8.x/eloquent#scope) untuk query yang simple
    tapi digunakan secara berulang
    * Apabila terdapat proses query yang rentan digunakan secara 
    bersamaan dalam waktu yang sangat singkat dapat menimbulkan server overload 
    (misal checkin, checkout, scan, presensi dll), gunakan PHP PDO
    * Gunakan events atau observer untuk menghandle
    broadcasting, hindari di controller
    * Gunakan Design Pattern Repository-MVC, supaya scalable
    * Gunakan abstraction class untuk proses yang membutuhkan lebih dari 2 kondisi
    * Buat helper class apabila ada fungsi yang digunakan lebih dari 2 kali
    * Manfaatkan caching query apabila memungkinkan 
    * Gunakan method eloquent `cursor()` apabila perlu mengambil data dari DB
    dengan validasi tertentu atau data tertentu
    * Wrap semua variable yang akan dikirimkan ke view dengan variable 
    array
    * Penamaan file model, kelas model dan value dari `protected $table` 
    berdasarkan nama tabel tetapi singular misal ada tabel bernama `students` 
    maka nama modelnya adalah `Student`
    * Penamaan variable menggunakan `snake_case`
    * Penamaan function menggunakan `camelCase`
    * Penamaan file class menggunakan `PascalCase`
    * Penamaan file blade menggunakan `snake_case`
    * Apabila nilai berupa booelan manfaatkan prefix `is_` misal `is_verified`
    * Apabila variable / function bersifat plural / array, gunakan akhiran (s / es)
1. Blade
    * Gunakan templating (`@extends`, `@section`, `@push`, `@stack`, `@include`) 
    dalam blade
    * Hindari kodingan `php` didalam blade file sebisa mungkin, gunakan helper class
    atau [blade directive](https://laravel.com/docs/6.x/blade#extending-blade) atau 
    [service injection](https://laravel.com/docs/8.x/blade#service-injection)
    * Jangan gunakan inline styling atau script yang panjang di dalam blade
    * Pecah file menjadi beberapa bagian (komponen) dengan tanggung jawab 
    yang berbeda - beda seperti misalnya pisahkan artikel dengan form kontak kami.
    * Pecah file yang memiliki `nested-if` dari suatu komponen / file utama (parent)
1. Database
    * Utamakan menggunakan bahasa inggris kecuali pada akronim
    * Prioritaskan gunakan relasi daripada hard-code / konstanta seperti pada case
    pengkategorian
    * Apabila nilai berupa booelan manfaatkan prefix `is_` atau `has_` misal `is_verified` atau `has_membership`
    * Gunakan `snake_case` dalam penamaan tabel dan kolom
    * Penamaan kolom `foreign_key` menggunakan format `{nama_tabel}_id` misal
    `mahasiswa_id`
    * Nama tabel selalu menggunakan akhiran s / es (plural)
    * Untuk penamaan pivot table, gunakan nama plural table pertama dan table kedua dengan pemisah `_` (underscore), misalnya `users_memberships`
1. UI / UX
    * Prioritaskan & biasakan gunakan link daripada teks biasa dimanapun
    * Tambahkan placeholder & helper text pada input untuk validasi dan keterangan tambahan
    * Gunakan tool seperti DataTable untuk tabel yang harus memuat banyak
    data
1. JavaScript
    * Sebisa mungkin jangan gunakan JQuery
    * Gunakan object variable seperti class
    (memiliki `getter`, `setter`, `onchangeevent`, dll) daripada variable biasa
    * Jangan gunakan attribute `onlick` pada element tapi tambahkan event listener
    * Penamaan variable menggunakan `camelCase`
    * Penamaan function menggunakan `camelCase`
    * Penamaan class menggunakan `PascalCase`
    * Penamanaan file menggunakan `kebab-case`
    * Apabila nilai berupa booelan manfaatkan prefix `is_` misal `is_verified`
    * Apabila variable / function bersifat plural / array, gunakan akhiran (s / es)
1. API
    * Penamaan variable menggunakan `snake_case`
    * Perhatikan pluralisasi variable, tambahkan `s` / `es` apabila merupakan array
    * Apabila nilai berupa booelan gunakan prefix `is_` misal `is_verified`
    * Response terdiri dari, yang kemudian disejajarnya ditambahkan respon / data yang perlu dikirimkan
    ```
    {
        code : HttpStatus,
        success : boolean,
        message : string,
    }
    ```
1. Routing
    * Jangan gunakan `id` dalam routing kecuali dengan tambahan attribute lain seperti
    `foo.com/bar/id`
    * Sebisa mungkin jangan gunakan lebih dari 1 kata pada tiap tiap slashnya, yang
    baik adalah `foo.com/user/client`, `foo.com/user/client/create`.
    Jangan `foo.com/user_client/create` atau `foo.com/user/client_create`
    * Gunakan keyword berikut sebagai referensi `create`,`store`,`update`,
    `delete`, `detail`, `show`, `edit`, `callback`
    * Jangan gunakan `id` sebagai routing usahakan tambahkan attribute lain sehingga
    bersifat lebih dinamis. Memanfaatkan `uid` diperbolehkan. Contoh routing
    yang baik adalah sebagai berikut `foo.com/profile/92/can` atau
    `foo.com/profile/can` atau `foo.com/profile/XR8l8OiW`
    * Jangan ada query pada URL
1. Git
    * Gunakan awalan berikut ketika melakukan commit
        * fix

            Berisi tentang perbaikan fitur / bug
        * remove
            
            Berisi tentang penghapusan file / suatu scope
        * add

            Berisi tentang penambahana fitur / penambahan suatu scope
        * update

            Berisi perubahan fitur / update fitur pada suatu scope
        * package

            Berisi tentang penambahan library / package / SDK
        * refactor

            Berisi tentang refactoring / optimisasi source code baik secara performa / readability
        * reformat

            Berisi tentang merapikan kode, seperti spacing & lining
    * Usahakan tiap perubahan yang ada di source code langsung di commit (Jangan menumpuk perubahan source code dalam 1 commit)
    * Gunakan forking repository